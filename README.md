# surrogates-GPs

The Surrogates by Robert Gramacy Homework Excercises solved using Python. Professor Gramacy uses R only, but the approaches to the examples and homework excercises amaze me, so I wanted to bring them to Python, in order to learn how to do this stuff on it too.

The book Surrogates is a nice exploration of the applications of Gaussian Processes, their model and design. It's more understandable than many other texts on the same topic.
